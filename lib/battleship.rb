require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    @board.print_grid
    pos = @player.get_play
    attack(pos)
  end

  def play_game(n=1)
    @board.place_random_ship(n)
    until game_over?
      play_turn
    end
  end
end

# pl = HumanPlayer.new("p")
# b = Board.new
# g = BattleshipGame.new(pl, b)
# g.play_game(6)
