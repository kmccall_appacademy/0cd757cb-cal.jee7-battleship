class Board
  attr_reader :grid

  def self.default_grid
    @grid = Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    grid.flatten.count(:s)
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def empty?(pos = nil)
    return false if pos == nil && count > 0
    return true if pos == nil && count == 0
    self[pos] == nil
  end

  def full?
    grid.flatten.length == count
  end

  def place_random_ship(n=1)
    raise "The ship is full!" if full?
    n.times do
      pos = random_pos
      self[pos] = :s
    end
  end

  def print_grid
    @grid.each { |row| puts row.to_s }
  end

  def won?
    count == 0
  end

  private

  def random_pos
    [rand(@grid.length), rand(@grid[0].length)]
  end

end
