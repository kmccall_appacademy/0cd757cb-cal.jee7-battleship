class HumanPlayer
  attr_reader :player

  def initialize(player)
    @player = player
  end

  def get_play
    puts "Make your move."
    pos_str = gets.chomp.to_s
    pos_string = pos_str.strip.split(",")
    pos_string.map(&:to_i)
  end

end
